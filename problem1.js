

function problem1(car_id, inventory){
    if(!car_id || (typeof car_id)!=='number'){
        return "Car_id is invalid."
    }

    
    let good_car = inventory.filter((element,index)=>{
        if("id" in element){
            return element.id === car_id;
        }
    })

    if(good_car.length===0){
        return `Car which has car_id - ${car_id} has not been found.`

    }
    let desired_car = good_car[0]


    return `Car ${car_id} is a ${("car_year" in desired_car )? desired_car.car_year : ''}, ${("car_make" in desired_car)? desired_car.car_make: ''}, ${("car_model" in desired_car) ? desired_car.car_model : ''}`
 
 
}


module.exports = problem1