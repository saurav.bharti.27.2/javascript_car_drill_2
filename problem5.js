const p4 = require('./problem4')



function problem5(inventory){
    try{
        const p5_arr = p4(inventory)
        if(!p5_arr){
            throw new Error("inventory is invalid")
        }
        const older_cars = p5_arr.filter((cars, index, p5_arr)=>{
            return cars<2000
        })
        
    
        if(older_cars)
            console.log(older_cars.length, " <- number of cars older than 2000")
        return older_cars;

    }catch(err){
        console.log(err.stack)
    }


}


module.exports= problem5