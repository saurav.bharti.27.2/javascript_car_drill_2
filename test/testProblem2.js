const problem2 = require('../problem2')
const inventory = require('../inventory')


try{
    if(!inventory){
        throw new Error("Inventory is not present")
    }
    const result = problem2(inventory)

    
}catch(err){
    console.log(err.stack)
}